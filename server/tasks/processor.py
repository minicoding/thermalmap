import os
from tasks.reader import ImageReader
from tasks.writer import ImageWriter


# TBD: Retry if it fails in the middle of the processing?
# TBD: Use multiple processors?


class Processor(object):
    def __init__(self, root_path, target, url_root):
        self.target = target if target != None and bool(target) else 'output'
        self.url_root = url_root

        # this is sort of like a database, for now.
        self.names = []
        self.data = {}
        self.hot_images = {}
        self.is_processed = False

        # tbd
        self.input_image_path = os.path.join(root_path, 'input')       # temporary
        self.output_image_path = os.path.join(root_path, self.target)  # temporary
        self.reader = ImageReader(self.input_image_path)
        self.writer = ImageWriter(
            self.input_image_path, self.output_image_path)

    def get_hot_image_file(self, name):
        '''Returns the path to the given image file'''
        if name in self.hot_images:
            return self.hot_images[name]
        else:
            return None

    def get_all_hot_images(self):
        '''Returns all hot image files'''
        # return self.hot_images.values()
        return self.hot_images


    # TBD: We just do sequential processing over all images, for the sake of simplicity.
    #      But, since all images are independent of each other,
    #      we can potentially parallize the processing and speed up the process.
    # Also, it'll be more efficient to process all files in one pass.
    #      The following algorithm is more readable, and it's a better starting point,
    #      but it reads the same images multiple times.
    #      We can always optimize it later.

    def process(self):
        self.__process_input_files()
        self.__process_temperature_data()
        self.__process_location_data()
        self.__copy_images_to_bins()
        self.is_processed = True

    def __process_input_files(self):
        self.names = self.reader.get_names()
        # print(self.names)
        count = len(self.names)
        print(f"Input image processed: count = {count}")

    def __process_temperature_data(self):
        for name in self.names:
            temp = self.reader.read_temperature(name)
            self.data[name] = {'temp': temp}

    def __process_location_data(self):
        for name in self.data:
            geo = self.reader.read_location(name)
            # print(geo)
            if geo != None and bool(geo):
                self.data[name]['geo'] = geo
            else:
                print(f"Error: failed to read location for {name}")

    def __copy_images_to_bins(self):
        self.hot_images = self.writer.copy_to_bins(self.data, self.url_root)


if __name__ == '__main__':
    cwd = os.path.dirname(os.path.realpath(__file__))
    root_path  = os.path.abspath(os.path.join(os.path.join(cwd, os.pardir), 'images'))
    print(f"root_path = {root_path}")
    processor = Processor(root_path, 'output1', 'http://localhost:5000/')
    processor.process()

    # print(processor.data)
    print(processor.get_all_hot_images())
