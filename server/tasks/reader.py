import os
import glob
# import exifread
from PIL import Image
from GPSPhoto import gpsphoto

class ImageReader(object):
    def __init__(self, path):
        self.path = path

        # tbd: these folder names are hard-coded for now.
        self.jpeg_folder = os.path.join(
            self.path, 'rjpeg_modified_0-80_gps_mod/*.jpg')
        self.tiff_folder = os.path.join(
            self.path, 'rjpeg_temperature_16bit_0-80/*.tif')

    def get_names(self) -> []:
        '''Returns the names of all files in the image folder (from both jpeg and tiff image folders).'''

        jpegNames = []
        for filename in glob.glob(self.jpeg_folder):
            name = os.path.splitext(os.path.basename(filename))[0]
            # print(name)
            jpegNames.append(name)

        tiffNames = []
        for filename in glob.glob(self.tiff_folder):
            name = os.path.splitext(os.path.basename(filename))[0]
            # print(name)
            tiffNames.append(name)

        # TBD: if jpegNames != tiffNames, throw error?
        # names = list(set(jpegNames) | set(tiffNames))
        names = list(set(jpegNames) & set(tiffNames))   # Since we need both jpeg and tiff images, intersection makes more sense.
        return names


    def read_temperature(self, name) -> int:
        '''Returns the highest pixel temperature associated with the tiff image file.'''

        file_path = os.path.join(
            self.path, f"rjpeg_temperature_16bit_0-80/{name}.tif")

        # temporary
        # print(f"Reading image {file_path}")
        img = Image.open(file_path, 'r')
        vals = list(img.getdata())
        # print(val)
        max_val = max(vals)
        return max_val


    def read_location(self, name) -> {}:
        '''Returns the location associated with the jpeg image file.'''

        file_path = os.path.join(
            self.path, f"rjpeg_modified_0-80_gps_mod/{name}.jpg")

        gpsinfo = gpsphoto.getGPSData(file_path)
        return  dict((k.lower(), v) for k,v in gpsinfo.items()) 

        # tags = exifread.process_file(open(file_path, 'rb'))
        # gpsinfo = {i:tags[i] for i in tags.keys() if i.startswith('GPS')}
        # return gpsinfo

        # gpsinfo = {}
        # for key in exif['GPSInfo'].keys():
        #     decode = ExifTags.GPSTAGS.get(key,key)
        #     gpsinfo[decode] = exif['GPSInfo'][key]
        # print(gpsinfo)


if __name__ == '__main__':
    pass
