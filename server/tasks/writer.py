import os
from shutil import copy, rmtree


class ImageWriter(object):
    def __init__(self, input_dir, output_dir):
        self.input_dir = input_dir
        self.output_dir = output_dir

        # tbd: these folder names are hard-coded for now.
        self.jpeg_dir = os.path.join(
            self.input_dir, 'rjpeg_modified_0-80_gps_mod')
        self.tiff_dir = os.path.join(
            self.input_dir, 'rjpeg_temperature_16bit_0-80')

        # 0: max(T) <= 0 degC
        # 1: 20 degC >= max(T) > 0 degC
        # 2: 40 degC >= max(T) > 20 degC
        # 3: 60 degC >= max(T) > 40 degC
        # 4: 80 degC >= max(T) > 60 degC
        # 5: max(T) > 80 degC

        self.bins = []
        for i in range(6):
            self.bins.append(os.path.join(self.output_dir, f"bin{i}"))

    # def get_jpeg_file(self, image_data, name):
    #     '''Returns the path to the given image file'''
    #     data = image_data[name]
    #     bin = self.bins[self.__get_bin_index(data['temp'])]
    #     jpg = os.path.join(bin, f"{name}.jpg")
    #     return jpg

    def copy_to_bins(self, image_data, url_root) -> []:
        '''Copy image files to bins based on the temperature. And, it returns the 'hot image' list.'''

        # create folders
        if os.path.exists(self.output_dir):
            rmtree(self.output_dir, ignore_errors=True)
        os.makedirs(self.output_dir)
        with open(os.path.join(self.output_dir, '.gitkeep'), 'w'):  # to make git happy
            pass

        for b in self.bins:
            os.makedirs(b)

        # copy files
        # and, keep track of all names with temperature > 60.
        hot_images = []
        for name, data in image_data.items():
            t = data['temp']
            index = self.__get_bin_index(t)
            bin = self.bins[index]
            jpg = os.path.join(self.jpeg_dir, f"{name}.jpg")
            copy(jpg, bin)
            tif = os.path.join(self.tiff_dir, f"{name}.tif")
            copy(tif, bin)
            # TBD: The "images" static url path is hard-coded.
            if index >= 4:
                hot = {'name': name,
                       'url': f"{url_root}{os.path.basename(self.output_dir)}/{os.path.basename(bin)}/{name}.jpg",
                       'geo': data['geo']}
                hot_images.append(hot)

        return hot_images

    def __get_bin_index(self, t: float) -> int:
        if t <= 0:
            return 0
        elif 0 < t <= 20:
            return 1
        elif 20 < t <= 40:
            return 2
        elif 40 < t <= 60:
            return 3
        elif 60 < t <= 80:
            return 4
        else:
            return 5


if __name__ == '__main__':
    pass
