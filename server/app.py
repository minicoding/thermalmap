import os
import json
from urllib.parse import urlparse
from flask import Flask, Request, request, jsonify, Response, make_response
from flask_restplus import fields, Resource, Api
from tasks.processor import Processor


app = Flask(__name__, static_url_path='/images', static_folder='images')
api = Api(app, version='0.0.1', title='Thermal Map',
          description='API for processing a set of thermal images stored on the server and returning the image coord list on a map, meeting certain conditions.',
          )

ns1 = api.namespace(
    'map', description='Process the image data and get the resulting geo information')

# processor map
# target -> Processor
processors = {}
last_proc = None

# tbd:
# we are relying on a particular directory structure.
# image_dir = app.instance_path
# image_dir = app.root_path
image_dir = os.path.abspath(os.path.join(app.root_path, 'images'))
print(f"image_dir = {image_dir}")


# Note: Flask-RestPlus seems to have some quirkiness in creating the swagger doc from doc comments.
#       I put each doc comment in one line.
@ns1.route('/', endpoint='thermal-map')
class LatestMap(Resource):
    '''The most recently processed map data.'''
    @ns1.doc(responses={
        200: 'Success',
        404: 'No data found',
        500: 'No map data available'
    })
    def get(self, **kwargs):
        '''Fetch the most recently processed map data. It returns 404 if no map data is found.'''
        if last_proc != None:
            if last_proc.is_processed:
                response = make_response(jsonify(last_proc.get_all_hot_images()), 200)
                response.headers.add('Access-Control-Allow-Origin', '*')
                return response
            else:
                return {'error': f"Image processing is pending"}, 500
        else:
            return {'error': f"Images have not been processed"}, 404



@ns1.route('/<string:target>', endpoint='map-processor')
@ns1.doc(params={'target': 'Input image folder (in the server file system). It should be a valid name for a directory on the server operating system.'})
class ImageProcessor(Resource):
    '''The map processor. Processes the thermal images on the server and puts the output in the target folder.'''

    # TBD: the original image folder param?
    # TBD: Return 202, and do the processing asynchronously?
    @ns1.doc(responses={
        204: 'Success',
        400: 'Request validation Error'
    })
    def post(self, target):
        '''Processes the thermal images in a predefined folder and stores the output in the specified target folder. If successful, it returns 204. If the target folder already exists, it will be overwritten, instead of returning an error.'''
        proc = None
        if target in processors:
            proc = processors[target]
        else:
            # url_root = request.url_root
            # url_root = request.base_url
            url_root = f"{request.host_url}images/"   # tbd: the static images folder path is hard-coded.
            proc = Processor(image_dir, target, url_root)
            processors[target] = proc
        # TBD: process this asynchronously??
        proc.process()
        global last_proc  # tbd
        last_proc = proc
        return '', 204

    # TBD: filter param? (e.g., to select specific bins, or temperature threshold, etc.)
    @ns1.response(200, 'Success')
    @ns1.response(404, 'Invalid target')
    @ns1.response(500, 'Processing error')
    def get(self, target, **kwargs):
        '''Fetch the map data in the specified target folder. If no such target exists, then it returns 404.'''
        if target not in processors:
            return {'error': f"Images have not been processed for output target {target}"}, 404
        else:
            proc = processors[target]
            if proc.is_processed:
                response = make_response(jsonify(proc.get_all_hot_images()), 200)
                response.headers.add('Access-Control-Allow-Origin', '*')
                return response
            else:
                return {'error': f"Image processing is pending for output target {target}"}, 500


if __name__ == '__main__':
    app.run(debug=True)
