import unittest
from tasks.reader import ImageReader

# python -m unittest tests.test_reader


class TestStringMethods(unittest.TestCase):
    def setUp(self):
        path = '/home/harry/Projects/minicoding/thermalmap/images/input'  # temporary
        self.reader = ImageReader(path)

    def test_get_names(self):
        names = self.reader.get_names()
        # print(names)
        count = len(names)
        print(f"count = {count}")
        self.assertEqual(count, 406)   # temporary

    def test_read_temperature(self):
        names = self.reader.get_names()
        self.assertGreaterEqual(len(names), 1)
        name = names[0]
        temp = self.reader.read_temperature(name)
        print(f"temperature = {temp}")

    def test_read_location(self):
        names = self.reader.get_names()
        self.assertGreaterEqual(len(names), 1)
        name = names[0]
        geo = self.reader.read_location(name)
        print(geo)


if __name__ == '__main__':
    unittest.main()
