// Example from https://uber.github.io/react-map-gl/#/Documentation/getting-started/get-started

import React, { Component } from 'react';
import ReactMapGL, { Marker, Popup } from 'react-map-gl';

class MapboxMap extends Component {

  state = {
    // Initial view
    // TBD: Set the initial view based on the data???

    // viewport: {   // SF bay area.
    //   width: 1200,
    //   height: 800,
    //   latitude: 37.7,
    //   longitude: -122.3,
    //   zoom: 10
    // },
    // viewport: {      // United states
    //   width: 1200,
    //   height: 800,
    //   latitude: 40,
    //   longitude: -95,
    //   zoom: 3
    // },
    viewport: {      // From the current data set. Hard-coded.
      width: 1200,
      height: 800,
      latitude: 51.7562,
      longitude: -100.896,
      zoom: 16
    },
    data: [],
    hotLocation: null,
  };

  componentDidMount() {
    // TBD: hard-coded, for now.
    const server = 'http://localhost:5000/';
    const mapEndpoint = `${server}map/`

    fetch(mapEndpoint)
      .then(response => response.json())
      .then(data => {
        this.setState({ data });
      })
      .catch(error => {
        console.log(`Error while fetching ${mapEndpoint}`, error);
      })
  }

  buttonClickHandler = (loc) => {
    console.log(`loc = `, loc)
    this.setState({ hotLocation: loc });
  }

  popupClickHandler = () => {
    this.setState({ hotLocation: null });
  }

  render() {
    return (
      <ReactMapGL
        mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
        // mapStyle="mapbox://styles/sideway/cjvgw3ea80u9g1dk6q8goj459"
        mapStyle="mapbox://styles/sideway/cjvgw3vrb0uea1cpnfxay1f6f"
        {...this.state.viewport}
        onViewportChange={(viewport) => this.setState({ viewport })}
      >
        {this.state.data.map(loc => (
          <Marker key={loc.name} latitude={loc.geo.latitude} longitude={loc.geo.longitude} offsetLeft={0} offsetTop={0}>
            <button className="marker-btn"
              onClick={e => {
                e.preventDefault();
                this.buttonClickHandler(loc);
              }}
            >
              <img src="flame-24.png" alt="^"></img>
            </button>
          </Marker>
        ))}

        {this.state.hotLocation ? (
          <Popup
            latitude={this.state.hotLocation.geo.latitude}
            longitude={this.state.hotLocation.geo.longitude}
            onClose={() => {
              this.popupClickHandler();
            }}
          >
            <div>
              <img src={this.state.hotLocation.url} alt={this.state.hotLocation.name}></img>
            </div>
          </Popup>
        ) : null}

      </ReactMapGL>
    );
  }
}

export default MapboxMap;
