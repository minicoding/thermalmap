import React from 'react';
import './App.css';
import MapboxMap from './mapbox-map';

function App() {
  console.log(`Environment = ${process.env.NODE_ENV}`);

  return (
    <div className="App">
      <h1>Hot Image Locations</h1>

      <MapboxMap></MapboxMap>

    </div>
  );
}

export default App;
