# Thermal Map

## Problem

Given a set of thermal images (e.g., "heatmaps"),
1. Classify those images into a set of bins based on their highest temperature values, and
1. Display their geolocations on a map if they belong to certain higher temperature bins.



## Procedure / Algorithm

* Create a folder with proper bins to store images.
* Create a database, or other internal data storage, to store the image metadata.
* Given a set of thermal images,
    * First, read those images,
    * "Parse" the images, and for each image
        * find the max temperature value in the image, and
        * read its associated GPS location (from exif).
    * (The parsed metadata and the target bin information is to be stored in the data store.) 
* Save all images into proper bins (based on their max temperature values).
* Create a javascript-based map.
* Place the images, or pins, on the map that belong to a predefined set of high-temperature bins ("hot images").



## Status

* Created a simple web service to trigger processing and fetch the result map data.
* Scaffolded a sample react app to host a client website for displaying the map.
* Implemented the logic for processing of the images and putting them into bins.
* Implemented the API for returning the "hot images".
* Added a mapbox map into the client react app.
* Now the client app calls the server API (running on http://localhost:5000) to get the list of hot images.
* Hot image locations are then displayed as markers on the map.


## To Do

* Need to investigate: Why are there 407 jpeg images and only 406 tiff images?
* (Also, there are a number of "TBD" comments throughout the code.)
* ...




## How to Run the Server and Test the API

Clone this repo. 

Cd to the `server` folder.

Copy the sample image data into the `images/input` folder
such that,
for example, `rjpeg_modified_0-80_gps_mod` is a child of the `input` folder.

The API service was built with _Flask_ and _flask-RESTPlus._
To run the server,

```
python app.py
```

Go to http://localhost:5000, and you will see the swagger doc.

You will need to POST first to trigger the image processing (with no request body).
You can use any string for the output `target` folder name. For example, "output" will do.
Once the processing is done, you can GET the list of the hot images using the same `target`.
Or, you can just GET without `target` to get the most recent result.


Example JSON response:

```
[
  {
    "name": "DJI_0075",
    "url": "http://localhost:5000/images/output3/bin4/DJI_0075.jpg",
    "geo": {
      "latitude": 51.75521874998889,
      "longitude": -100.89999825000001,
      "altitude": 1293
    }
  },
  ...
]
```


To trigger the processing without using the web API,
just run the `Processor` script.
For example,

```
python -m tasks.processor
```



### Notes

(A) Some paths are hard-coded in the app.
If the API does not return the valid response,
you may have to tweak these paths in the code.


(B) The server was developed with python 3.6.7 on Ubuntu.

My `pip list` output (on venv):

```
aniso8601 (6.0.0)
astroid (2.2.5)
attrs (19.1.0)
autopep8 (1.4.4)
Click (7.0)
ExifRead (2.1.2)
Flask (1.0.2)
flask-restplus (0.12.1)
gpsphoto (2.2.2)
isort (4.3.18)
itsdangerous (1.1.0)
Jinja2 (2.10.1)
jsonschema (3.0.1)
lazy-object-proxy (1.3.1)
MarkupSafe (1.1.1)
mccabe (0.6.1)
piexif (1.1.2)
Pillow (6.0.0)
pip (9.0.1)
pkg-resources (0.0.0)
pycodestyle (2.5.0)
pylint (2.3.1)
pyrsistent (0.15.1)
pytz (2019.1)
setuptools (39.0.1)
six (1.12.0)
typed-ast (1.3.5)
Werkzeug (0.15.2)
wrapt (1.11.1)
```




## How to Build and Run the Client App

The client-side react app runs on http://localhost:3000/ 
and it talks to the API server running on http://localhost:5000/


Cd to the client directory. Then,

```
npm i
```

First, make sure that the server is running.
And, call the POST endpoint to generate some output (images classified into bins).

Set the enviroment variable `REACT_APP_MAPBOX_TOKEN` to your mapbox access token,
and run

```
npm run start
```

You should be able to see the "hot image" markers on the map.

